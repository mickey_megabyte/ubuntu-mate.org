<!-- 
.. title: Community
.. slug: community
.. date: 2014-06-10 23:01:09 UTC
.. tags: Ubuntu,MATE,community,Google+,Twitter,Launchpad
.. link: 
.. description: 
.. type: text
-->

We have a number of ways you can connect with the Ubuntu MATE community.

## Discussion

If you want to chat with the Ubuntu MATE team and other members of our
growing community, this is where to go.

<div class="bs-component">
    <div class="jumbotron">
        <h1>Ubuntu MATE Community Forum</h1>
        <p>The best place to play an active role in the Ubuntu MATE community
        and to help shape it's direction is via our forum.</p>
        <a href="https://ubuntu-mate.community" class="btn btn-primary btn-lg">Forum</a>
    </div>
</div>

### Chat
 
Ubuntu MATE also has it's own IRC channel, [#ubuntu-mate](/irc/), on the [Freenode](http://freende.net)
network. The [#ubuntu-mate](/irc/) channel is also home to *matey*, probably the
smartest IRC bot in the world, EVER!

## The social networks

Ubuntu MATE is active on the following social networks.

<div class="row">
  <div class="col-lg-4">
    <div class="well bs-component text-center">
    <a class="social-icon" href="https://plus.google.com/communities/108331279007926658904" title="Ubuntu MATE Google+"><img class="centered" src="/assets/img/google+-48x48.png" alt="Ubuntu MATE Google+"></a>
    <p><b>Google+</b></p>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="well bs-component text-center">
    <a class="social-icon" href="https://twitter.com/ubuntu_mate" title="Ubuntu MATE Twitter"><img class="centered" src="/assets/img/twitter-48x48.png" alt="Ubuntu MATE Twitter"></a>
    <p><b>Twitter</b></p>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="well bs-component text-center">
    <a class="social-icon" href="https://www.facebook.com/UbuntuMATEedition/" title="Ubuntu MATE Facebook"><img class="centered" src="/assets/img/facebook-48x48.png" alt="Ubuntu MATE Facebook"></a>
    <p><b>Facebook</b></p>
    </div>
  </div>
</div>

## Development

This is where you can get involved with Ubuntu MATE and MATE Desktop development.

<div class="row">
  <div class="col-lg-3">
    <div class="well bs-component text-center">
    <a class="social-icon" href="https://launchpad.net/ubunu-mate/" title="Ubuntu MATE Launchpad"><img class="centered" src="/assets/img/logos/launchpad.png" alt="Ubuntu MATE Launchpad"></a>
    <p><b>Ubuntu MATE Launchpad</b></p>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="well bs-component text-center">
    <a class="social-icon" href="https://bitbucket.org/ubuntu-mate" title="Ubuntu MATE Bitbucket"><img class="centered" src="/assets/img/logos/bitbucket.png" alt="Ubuntu MATE Bitbucket"></a>
    <p><b>Ubuntu MATE Bitbucket</b></p>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="well bs-component text-center">
    <a class="social-icon" href="https://github.com/mate-desktop/" title="MATE Desktop GitHub"><img class="centered" src="/assets/img/logos/github.png" alt="MATE Desktop GitHub"></a>
    <p><b>MATE Desktop GitHub</b></p>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="well bs-component text-center">
    <a class="social-icon" href="https://www.transifex.com/projects/p/MATE/" title="MATE Desktop Transifex"><img class="centered" src="/assets/img/logos/transifex.jpg" alt="MATE Desktop Transifex"></a>
    <p><b>MATE Desktop Transifex</b></p>
    </div>
  </div>  
</div>
