<!-- 
.. title: Donate
.. slug: donate
.. date: 2014-11-05 23:01:09 UTC
.. tags: Ubuntu,MATE,donate
.. link: 
.. description: 
.. type: text
.. author: Martin Wimpress
-->

<style>
img.centered {
    display: block;
    margin-left: auto;
    margin-right: auto }
</style>

This funding campaign has three main objectives: 

  * Fund the the Ubuntu MATE project's hosting and bandwidth costs. This include domain name registrations, server hosting and bandwidth costs to provide a website and Discourse powered community space that is not reliant on social networks.
  * Fund the full time development of Ubuntu MATE and MATE Desktop and also be able to offer financial rewards to contributors of either project. Help subsidise the travel expenses of team members to represent MATE Desktop and Ubuntu MATE at FLOSS conferences.
  * Support other Open Source projects that Ubuntu MATE depends upon.

Patrons will be rewarded with exclusive project news, updates and invited to
participate in video conferences where you can talk to the developers directly.

We have set up a number of payment options that should hopefully suit everyone.
Go on, be brilliant, help grow our community.

## Patreon

This is a unique way to fund an Open Source community. A regular monthly income
for the Ubuntu MATE project will allow us to better plan how best to reward our
contributors and sponsor other Open Source projects.

<div class="bs-component">
    <div class="jumbotron">
        <h1>Monthly supporter</h1>
        <p>Become a monthly supporter at <a href="http://www.patreon.com/ubuntu_mate">Patreon</a>
        and help grow the Ubuntu MATE community.</p>
        <a href="http://www.patreon.com/ubuntu_mate" class="btn btn-primary btn-lg">Become a Patron</a>
        </p>
    </div>
</div>

## PayPal

<img class="right" src="https://www.paypalobjects.com/webstatic/mktg/Logo/pp-logo-100px.png" alt="PayPal Logo">

We'd prefer you set-up a recurring payment to help ensure the monthly
costs are always covered and it also allows us to better plan how to
reward our contributors and sponsor other Open Source projects.
That said, one time donations are also gratefully accepted.

**NOTE! If you are considering a monthly recurring payment via PayPal then
you should probably use the [Ubuntu MATE Patreon](http://www.patreon.com/ubuntu_mate)
to benefit from Patron rewards.**

<div class="bs-docs-section">
  <div class="row">
    <div class="col-lg-6">
      <div class="well bs-component">
        <form name="monthly" class="form-horizontal" action="https://www.paypal.com/cgi-bin/webscr" onsubmit="return validateMonthlyForm()" method="post">
          <fieldset>
            <legend>Monthly PayPal supporter</legend>
            <div class="form-group">
              <label for="donationAmount" class="col-lg-4 control-label">Select an amount</label>
              <div class="col-lg-6">
                <input type="radio" name="amt" value="2.50">2.50
                <input type="radio" name="amt" value="5" checked="">5
                <input type="radio" name="amt" value="10">10
              </div>
            </div>          
            <div class="form-group">
              <label for="specifyAmount" class="col-lg-4 control-label">Specify your own amount</label>
              <div class="col-lg-6">      
                <input type="text" name="other" value="" size="5" maxlength="5">              
              </div>
            </div>
            <div class="form-group">
              <label for="select" class="col-lg-4 control-label">Currency</label>
              <div class="col-lg-6">
                <select class="form-control" name="currency_code">
                  <option>EUR</option>
                  <option>USD</option>
                  <option selected="">GBP</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-6">
                <button type="submit" class="btn btn-primary">Monthly Donation</button>
              </div>
            </div>
          </fieldset>
          <input type="hidden" name="cmd" value="_xclick-subscriptions">
          <input type="hidden" name="business" value="6282B4CZGVCB6">
          <input type="hidden" name="item_name" value="Ubuntu MATE Monthly Supporter">
          <input type="hidden" name="no_shipping" value="1">
          <input type="hidden" name="no_note" value="1">
          <input type="hidden" name="charset" value="UTF-8">
          <input type="hidden" name="a3" value="">
          <input type="hidden" name="p3" value="1">
          <input type="hidden" name="t3" value="M">
          <input type="hidden" name="src" value="1">
          <input type="hidden" name="sra" value="1">
          <input type="hidden" name="return" value="https://ubuntu-mate.org/donation-completed/">
          <input type="hidden" name="cancel_return" value="https://ubuntu-mate.org/donation-cancelled/">
        </form>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="well bs-component">
        <form name="single" class="form-horizontal" action="https://www.paypal.com/cgi-bin/webscr" onsubmit="return validateSingleForm()" method="post">
          <fieldset>
            <legend>One time PayPal donation</legend>
            <div class="form-group">
              <label for="donationAmount" class="col-lg-4 control-label">Select an amount</label>
              <div class="col-lg-6">
                <input type="radio" name="amt" value="2.50">2.50
                <input type="radio" name="amt" value="5" checked="">5
                <input type="radio" name="amt" value="10">10
              </div>
            </div>
            <div class="form-group">
              <label for="specifyAmount" class="col-lg-4 control-label">Specify your own amount</label>
              <div class="col-lg-6">      
                <input type="text" name="other" value="" size="5" maxlength="5">              
              </div>
            </div>
            <div class="form-group">
              <label for="select" class="col-lg-4 control-label">Currency</label>
              <div class="col-lg-6">
                <select class="form-control" name="currency_code">
                  <option>EUR</option>
                  <option>USD</option>
                  <option selected="">GBP</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-6">
                <button type="submit" class="btn btn-primary">Single Donation</button>
              </div>
            </div>
          </fieldset>
          <input type="hidden" name="cmd" value="_xclick">
          <input type="hidden" name="business" value="6282B4CZGVCB6">
          <input type="hidden" name="item_name" value="Ubuntu MATE One-time Donation">
          <input type="hidden" name="no_shipping" value="1">
          <input type="hidden" name="no_note" value="1">
          <input type="hidden" name="charset" value="UTF-8">
          <input type="hidden" name="amount" value="">
          <input type="hidden" name="src" value="1">
          <input type="hidden" name="sra" value="1">
          <input type="hidden" name="return" value="https://ubuntu-mate.org/donation-completed/">
          <input type="hidden" name="cancel_return" value="https://ubuntu-mate.org/donation-cancelled/">
        </form>  
      </div>
    </div>
  </div>
</div>

# Commercial sponsorship

If you are using, or plan to use, Ubuntu MATE in a commercial environment
and would like to sponsor the project more formally then please head over
to our Patreon page where we have commercial sponsorship packages.

<div class="bs-component">
    <div class="jumbotron">
        <h1>Commercial sponsor</h1>
        <p>Become a commercial sponsor at <a href="http://www.patreon.com/ubuntu_mate">Patreon</a>
        and have your business prominently listed as a sponsor.</p>
        <a href="http://www.patreon.com/ubuntu_mate" class="btn btn-primary btn-lg">Become a commercial sponsor</a>
        </p>
    </div>
</div>

## Bitcoin and micro payments

<div class="bs-docs-section">
  <div class="row">
    <div class="col-lg-6">
      <div class="well bs-component">
        <legend>Bitcoin</legend>
          <p>Click or scan to QR code below to launch your Bitcoin client and
          donate 0.02 btc to Ubuntu MATE. Alternatively, copy and paste the
          Bitcoin address into your Bitcoin client to donate an amount of your
          choice.</p>
          <p align="center">
            <a href="bitcoin:1Mpan6eExzNKdS8JnFAod5Pwt49aR6JsDB?amount=0.02&label=Ubuntu%20MATE">
            <img src="https://chart.googleapis.com/chart?chs=192x192&cht=qr&chl=bitcoin:1Mpan6eExzNKdS8JnFAod5Pwt49aR6JsDB?amount=0.02&message=Donate_0.02_btc_to_Ubuntu_MATE" /></a>
            <br />
            <tt>1Mpan6eExzNKdS8JnFAod5Pwt49aR6JsDB</tt>
          </p>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="well bs-component">
        <legend>Flattr</legend>
          <p>Maybe you prefer to use a micro payment service? With
          <a href="https://flattr.com/howflattrworks" target="_blank">Flattr</a>,
          supporting creators becomes a natural part of life. Paying for content
          does not only feel good, it makes the world a better place.</p>
          <p align="center">
            <script id='fbjjn1d'>(function(i){var f,s=document.getElementById(i);f=document.createElement('iframe');f.src='//api.flattr.com/button/view/?uid=ubuntumatedotorg&url='+encodeURIComponent(document.URL);f.title='Flattr';f.height=62;f.width=55;f.style.borderWidth=0;s.parentNode.insertBefore(f,s);})('fbjjn1d');</script>
          </p>
      </div>      
    </div>
  </div>
</div>

# Summary of donations

We post an overview of how the donations were used each month.

  * [2014 November](https://ubuntu-mate.org/blog/ubuntu-mate-november-2014-supporters/)
  * [2014 December](https://ubuntu-mate.org/blog/ubuntu-mate-december-2014-supporters/)
  * [2015 January](https://ubuntu-mate.org/blog/ubuntu-mate-january-2015-supporters/)
  * [2015 February](https://ubuntu-mate.org/blog/ubuntu-mate-february-2015-supporters/)
  * [2015 March](https://ubuntu-mate.org/blog/ubuntu-mate-march-2015-supporters/)
  * [2015 April](https://ubuntu-mate.org/blog/ubuntu-mate-april-2015-supporters/)
  * [2015 May](https://ubuntu-mate.org/blog/ubuntu-mate-may-2015-supporters/)  
  * [2015 June](https://ubuntu-mate.org/blog/ubuntu-mate-june-2015-supporters/)  

<script type="text/javascript">
  function validateMonthlyForm() {
    var n = document.forms["monthly"]["other"].value;
      if (n) {
        if (!isNaN(parseFloat(n)) && isFinite(n) && (n > 0)) {
          document.forms["monthly"]["a3"].value = n;
          return true;
        } else {
          alert("Please enter a valid donation amount - thanks!");
          document.forms["monthly"]["other"].value = "";
          return false;
        }
      }
      else {
        document.forms["monthly"]["a3"].value = document.forms["monthly"]["amt"].value;
        return true;
      }
  }

  function validateSingleForm() {
    var n = document.forms["single"]["other"].value;
      if (n) {
        if (!isNaN(parseFloat(n)) && isFinite(n) && (n > 0)) {
          document.forms["single"]["amount"].value = n;
          return true;
        } else {
          alert("Please enter a valid donation amount - thanks!");
          document.forms["single"]["other"].value = "";
          return false;
        }
      }
      else {
        document.forms["single"]["amount"].value = document.forms["single"]["amt"].value;
        return true;
      }
  }  
</script>
