<!-- 
.. title: FAQ	
.. slug: faq
.. date: 2014-06-10 23:01:09 UTC
.. tags: Ubuntu,MATE
.. link: 
.. description: 
.. type: text
.. author: Martin Wimpress
-->

If we get asked something frequently enough we'll try and provide 
the answers here.

## What is Ubuntu MATE exactly?

See our [What is Ubuntu MATE?](/what-is-ubuntu-mate/) page.

## Is Ubuntu MATE an official Canonical project?

No, it is an Ubuntu community project.

## OK, so *who is* behind Ubuntu MATE?

Take a look at [our team](/team/) page to see the all-star cast who 
have made Ubuntu MATE possible.

## Is Ubuntu MATE an official Ubuntu *"flavour"*?

  * Ubuntu MATE 15.04 and onward are **official** Ubuntu flavours.
  * Ubuntu MATE 14.04 and Ubuntu MATE 14.10 are *unofficial* builds.

## Will you be making a 14.04 LTS version of Ubuntu MATE?

We have, although it is not an official build. You can get it here:

  * [Ubuntu MATE 14.04.2 LTS](https://ubuntu-mate.org/trusty/)

We have also posted [a HOW-TO for installing your own Ubuntu MATE 14.04 from scratch](/blog/2014-08-ubuntu-mate-14-04-from-scratch/).

## Why is *xyz* application installed by default when I prefer *abc* application?

In general the default application selection follows what shipped in 
Ubuntu 14.04. The only reason we deviate from the default Ubuntu 
applications is if they are not compatible with Ubuntu MATE or pull in
lots of unnecessary dependencies.

The reason for using the default applications as shipped by Ubuntu is 
the Ubuntu staff support the default applications. The Ubuntu MATE team 
is rather small so sharing the support responsibility makes sense, both 
for us and our users.

## Why is *xyz* application installed default when *abc* application is better?

See the answer to the question above.

## I've got a question. Where do I ask?

Ask the [Ubuntu MATE community](/community/).

## What has Ubuntu MATE got planned for the future?

Hopefully our [roadmap](/roadmap/) outlines some of what we have
planned. If you have ideas, we'd love to hear from you.
