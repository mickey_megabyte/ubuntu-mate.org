<!--
.. title: Ubuntu MATE Sponsors
.. slug: sponsors
.. date: 2015-05-13 20:32:42 UTC
.. tags: Ubuntu,MATE,sponsors,donate
.. link: 
.. description: Ubuntu MATE sponsors and patrons.
.. type: text
.. author: Martin Wimpress
-->

Ubuntu MATE is kindly supported by these fine sponsors and our awesome Patrons.

## Commercial sponsors

<div class="row">
  <div class="col-lg-4">
    <div class="well bs-component">
    <a href="https://entroware.com"><img class="centered" src="/assets/img/sponsors/entroware.png" alt="Entroware" /></a>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="well bs-component">
    <a href="http://www.libretrend.com/en/"><img class="centered" src="/assets/img/sponsors/libretrend.png" alt="LibreTrend" /></a>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="well bs-component">
    <a href="https://www.prometeus.net/billing/aff.php?aff=239"><img class="centered" src="/assets/img/sponsors/prometeus.png" alt="Prometeus" /></a>
    </div>
  </div>
</div>

## Patrons

Many thanks to the following people for becoming Ubuntu MATE patrons who collectively donated **$366.0** this month.

### The following Patrons contribute $10, or more, every month.

<table class="table table-striped table-hover">
  <thead>
    <tr>
      <th>Patrons</th>
      <th>Joined</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Poul LeBlanc</td>
      <td>2015-01-17 19:21:40</td>
    </tr>
    <tr>
      <td>Robert Boudreau</td>
      <td>2015-03-08 21:01:28</td>
    </tr>
    <tr>
      <td><a href="https://twitter.com/Entroware">Entroware</a></td>
      <td>2015-03-07 03:44:29</td>
    </tr>
    <tr>
      <td>Alex Alexzander</td>
      <td>2015-04-11 03:21:36</td>
    </tr>
    <tr>
      <td><a href="https://twitter.com/spazmaticcelery">SpazzyC</a></td>
      <td>2014-11-10 20:32:27</td>
    </tr>
    <tr>
      <td>Chris Gardiner-Bill</td>
      <td>2015-01-04 22:47:21</td>
    </tr>
    <tr>
      <td>Davide Monge</td>
      <td>2015-03-11 22:57:13</td>
    </tr>
    <tr>
      <td><a href="https://twitter.com/dirkjanvdhoorn">Dirk-Jan van der Hoorn</a></td>
      <td>2015-02-21 13:18:16</td>
    </tr>
    <tr>
      <td>Dave Hills</td>
      <td>2015-05-14 18:12:54</td>
    </tr>
    <tr>
      <td>Andrew Neher</td>
      <td>2015-05-14 13:08:08</td>
    </tr>
  </tbody>
</table>
<br />
### The following Patrons also contribute every month.

<small>David Hollings, Robert Meineke, Joe, <a href="https://twitter.com/nadrimajstor">Ivan Pejić</a>, DoctorKraz, Sergio Rivera, Patrik Nilsson, <a href="https://twitter.com/parzzix">Tim Apple</a>, Michael DiFazio, Silas Wulff, Jack Blakely, Adrian Evans, Matt Hartley, M Hoppes, Antoine Mate Messiah, Daniel Neilson, Scott Petty, Sascha Spettmann, david van Dyk, Arthur Vasquez, Mark Boadey, Christopher atkins, Stephen Donovan, <a href="https://twitter.com/ifollowyou">Cato Gaustad</a>, Stephen Cook, Tavis Gillard, Atreju, <a href="https://twitter.com/taksuyu">Tak Suyu</a>, Bob Wright, Specops872, Krishna, Paul Howarth, Jordan Hopkins, <a href="https://twitter.com/ebeyer">Eric</a>, Michael McGuire, Michael White, Lukasz, Francisco López Riojas, <a href="https://twitter.com/magnuslindstrom">gnusd</a>, Yannick Kooistra, Richard Arnold, Jason Hyder, Gaius, </small><br />
### The *Unlucky* Patron!

Darrell Vermilion is a good sport! They have chosen to be the Ubuntu MATE *unlucky* Patron, just so they can see their name in flashing lights.

<div align="center">
<h2><blink>Darrell Vermilion</blink><h2>
</div>

<div class="bs-component">
    <div class="jumbotron">
        <h1>Monthly supporter</h1>
        <p>Become a monthly supporter at <a href="http://www.patreon.com/ubuntu_mate">Patreon</a>.
        Patrons get exclusive project updates, invites to live video conferences with the Ubuntu
        MATE developers and discounts on some Ubuntu MATE merchandise.</p>
        <a href="http://www.patreon.com/ubuntu_mate" class="btn btn-primary btn-lg">Become a Patron</a>
        </p>
    </div>
</div>

<script type="text/javascript">
  setInterval(function(){
      $('blink').each(function(){
        $(this).css('visibility' , $(this).css('visibility') === 'hidden' ? '' : 'hidden')
      });
    }, 250);
</script>
