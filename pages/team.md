<!-- 
.. title: Team
.. slug: team
.. date: 2014-06-10 23:01:09 UTC
.. tags: Ubuntu,MATE
.. link: 
.. description: 
.. type: text
-->

Here is the troupe of all star international caberet artists responsible
for making Ubuntu MATE a thing. If you'd like to get involved
[then please let us know](/community/).

## Founders

The idiots who thought Ubuntu MATE would be a good idea.

  * [Martin Wimpress](http://flexion.org) Project co-founder. [MATE Desktop](http://mate-desktop.org) developer and Ubuntu MATE project lead.
  * [Alan Pope](http://popey.com) Project co-founder. [Canonical](http://www.canonical.com) and [Ubuntu](http://www.ubuntu.com) liaison. Enthusiam co-ordinator.

## Developers

Members of the core MATE Desktop development team who have been helping
develop and test Ubuntu MATE.

  * [Stefano Karapetsas](http://blog.karapetsas.com/) Lead developer of [MATE Desktop](http://mate-desktop.org). [Debian](http://www.debian.org) and [Ubuntu](http://www.ubuntu.com) maintainer.
  * [Sander Sweers](https://github.com/infirit/) Developer of [MATE Desktop](http://mate-desktop.org) and magician of everything.
  * [Vlad Orlov](https://github.com/monsta) Developer of [MATE Desktop](http://mate-desktop.org) and bug fix king.

## Maintainers

The heroes from the Debian packaging team. Without their tireless efforts
there would be no Ubuntu MATE.

  * [Mike Gabriel](http://sunweavers.net/blog/) Package maintainer for [Debian](http://www.debian.org).
  * [Steve Zesch](https://github.com/szesch) Package maintainer for [Debian](http://www.debian.org).
  * [John Paul Adrian Glaubitz](http://users.physik.fu-berlin.de/~glaubitz/) Package maintainer for [Debian](http://www.debian.org).
  * [Vangelis Mouhtsis](https://github.com/gnugr) Package maintainer for [Debian](http://www.debian.org).

## Designers and Artists

The art department and creators of beautiful things.

  * [Goce Mitevski](http://nicer2.com) Designer, digital artist and desktop decorator.
  * [Ivan Pejić](https://plus.google.com/113587242852192152625/) Digital artist, perfectionist and aspiring designer.
  * [Jack Mohegan](https://plus.google.com/101312215214323407176/) Digital artist and theme tweaker.
  * [Michael Tunnell](http://michaeltunnell.com/) Website and graphic designer.
  * [Riccardo Pecchioli](https://plus.google.com/104108115467526996500) Digital video animator.
  * [Sam Hewitt](http://snwh.org/) Designer and creative genius.

## Architecture porters

These fine people are bringing Ubuntu MATE to other hardware platforms.

  * [Adam Smith](https://plus.google.com/u/0/111285327879595317710) Ubuntu MATE for PowerPC creator.
  * [Rohith Madhavan](https://ubuntu-mate.community/users/rohithmadhavan) Ubuntu MATE for Raspberry Pi 2 creator.

## Communications

These are the social animals of the Ubuntu MATE community who help spread
the word.

  * [Brett Wiley](https://plus.google.com/+BrettWiley) G+ community moderator.  
  * [Caleb Howland](http://wiki.ubuntu.com/SonikkuAmerica) Facebook page administrator.

## Contributors

The following people have made infrequent, yet significant, contributions
to the Ubuntu MATE project.

  * [Jonathan Nadeau](http://jnadeau.org/) assistive technology advisor and accessibility testing.
