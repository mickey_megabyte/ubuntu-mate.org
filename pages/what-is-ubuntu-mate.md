<!-- 
.. title: What is Ubuntu MATE?
.. slug: what-is-ubuntu-mate
.. date: 2014-08-24 23:03:09 UTC
.. tags: Ubuntu,MATE
.. link: 
.. description: 
.. type: text
-->

If you are new to Ubuntu MATE, or only casually acquainted with Linux 
based distributions, it can be difficult to understand how a Linux 
operating system compares with other computer systems that you may 
already be familiar with. Hopefully this page with help demystify 
Ubuntu MATE for new comers.

## Ubuntu

<img class="left" src="/assets/img/logos/ubuntu-logo32.png" alt="Ubuntu" width="96" height="96">

[Ubuntu](http://www.ubuntu.com) is one of, if not the, largest deployed 
[Linux](http://www.linux.com) based desktop operating systems in the 
world. Linux is at the heart of Ubuntu and makes it possible to create 
secure, powerful and versatile operating systems, such as Ubuntu and 
[Android](http://www.android.com/). Android is now in the hands of 
billions of people around the world and it's also powered by Linux.

Ubuntu is available in a number of different *"flavours"*, each coming 
with its own desktop environment. Ubuntu MATE takes the Ubuntu base 
operating system and adds the [MATE Desktop](http://mate-desktop.org).

## MATE Desktop

Wikipedia says that [a Desktop Environment is](http://en.wikipedia.org/wiki/Desktop_environment):

  > an implementation of the desktop metaphor made of a bundle of 
  programs running on top of a computer operating system, which share a 
  common graphical user interface (GUI). Desktop GUIs help the user to 
  easily access and edit files.

<img class="right" src="/assets/img/logos/mate-logo.png" alt="Ubuntu" width="112" height="112">

The MATE Desktop is one such implementation of a desktop environment 
and includes a file manager which can connect you to your local and 
networked files, a text editor, calculator, archive manager, image 
viewer, document viewer, system monitor and terminal. All of which are 
highly customisable and managed via a control centre.

MATE Desktop provides an intuitive and attractive desktop environment 
using traditional metaphors, which means if you've ever used
[Microsoft Windows](http://www.microsoft.com) or [Apple Mac 
OS](http://www.apple.com) it will feel very familiar.

The MATE Desktop has a rich history and is the continuation of the 
GNOME2 desktop, which was the default desktop environment on many Linux 
and Unix operating systems for over a decade. This means that MATE 
Desktop is tried, tested and very reliable.

## Powerful Applications

While MATE Desktop provides the essential user interfaces to control 
and use a computer, Ubuntu MATE adds a collection of additional 
applications to turn your computer into a truly powerful workstation. 

### Productivity

<div class="row">
  <div class="col-lg-4">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active" href="https://www.mozilla.org/firefox/desktop/">Firefox</a>
        <p class="list-group-item"><img class="centered" src="https://apps.ubuntu.com/site_media/icons/2014/08/firefox_25.png"></p>
        <p class="list-group-item">Safe and easy web browser</p>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active" href="https://www.mozilla.org/thunderbird/features/">Thunderbird</a>
        <p class="list-group-item"><img class="centered" src="https://apps.ubuntu.com/site_media/icons/2014/08/thunderbird_25.png"></p>
        <p class="list-group-item">Email client with integrated spam filter</p>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active" href="http://www.libreoffice.org/">LibreOffice</a>
        <p class="list-group-item"><img class="centered" src="https://apps.ubuntu.com/site_media/icons/2014/08/libreoffice4.0gettingstartedcvqpMo.png"></p>
        <p class="list-group-item">Full-featured office productivity suite that is Microsoft(R) Office compatible.</p>
      </div>
    </div>
  </div>
</div>

### Entertainment

<div class="row">
  <div class="col-lg-4">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active" href="https://wiki.gnome.org/Apps/Rhythmbox">Rhythmbox</a>
        <p class="list-group-item"><img class="centered" src="https://apps.ubuntu.com/site_media/icons/2014/08/rhythmbox_25.png"></p>
        <p class="list-group-item">Music player and organiser</p>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active" href="https://wiki.gnome.org/Apps/Shotwell">Shotwell</a>
        <p class="list-group-item"><img class="centered" src="https://apps.ubuntu.com/site_media/icons/2013/06/shotwell_4.png"></p>
        <p class="list-group-item">Digital photo organiser.</p>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active" href="www.videolan.org/vlc/">VLC</a>
        <p class="list-group-item"><img class="centered" src="https://apps.ubuntu.com/site_media/icons/2014/11/vlc_4.png"></p>
        <p class="list-group-item">Play, capture, broadcast your multimedia streams.</p>
      </div>
    </div>
  </div>
</div>

Naturally you'll also find a firewall, backup application, 
document/photo scanner and printer management all included in Ubuntu 
MATE. And this is just the start. The [Ubuntu Software Centre](http://apps.ubuntu.com)
includes thousands of applications suitable for just about any
professional or recreational pursuit.

## Games

<img class="left" src="/assets/img/logos/steam-logo.png" alt="Steam" width="112" height="112">

In the last couple of years Linux has become a first class gaming
platform thanks to [Valve](http://www.valvesoftware.com/) bringing the
[Steam](http://store.steampowered.com/) platform to Linux. At the time
of writing Steam has more than 1000 high quality indie and AAA titles
available for Linux. Ubuntu MATE is fully compatible with Steam for
Linux.

While Steam is a major step forward for gaming on Linux there are also 
many high quality and enjoyable Open Source games titles available 
for Ubuntu MATE via the Ubuntu Software Centre. It doesn't matter if 
you like flight simulators, motor racing, first person shooters, jump
and run or card games you'll find something to keep you entertained.

## Open Source

<img class="right" src="/assets/img/logos/OSI-logo-300x352.png" alt="Open Source Initiative" width="112" height="131">

What Linux, Ubuntu and MATE Desktop all have in common is they are 
Open Source. Open source software is software that can be freely used, 
changed, and shared (in modified or unmodified form) by anyone.

In a nut shell Ubuntu MATE is free, in the truest sense of the word.
